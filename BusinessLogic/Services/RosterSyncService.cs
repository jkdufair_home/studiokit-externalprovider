﻿using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Diagnostics;
using StudioKit.ErrorHandling.Exceptions;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.Properties;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.BusinessLogic.Services
{
	public class RosterSyncService<TUser, TGroup, TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser> : IRosterSyncService
		where TUser : class, IUser, new()
		where TGroup : class, IGroup<TGroupUserRole, TExternalGroup>, new()
		where TGroupUserRole : class, IGroupUserRole, new()
		where TGroupUserRoleLog : class, IGroupUserRoleLog, new()
		where TExternalGroup : class, IExternalGroup, new()
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
	{
		private readonly IErrorHandler _errorHandler;
		private readonly ILogger _logger;
		private readonly IRosterSyncDbContext<TUser, TGroup, TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser> _dbContext;
		private readonly IDictionary<Type, Func<IRosterProviderService<TExternalGroup, TExternalGroupUser>>> _rosterProviderServiceMapping;

		public RosterSyncService(
			IErrorHandler errorHandler,
			ILogger logger,
			IRosterSyncDbContext<TUser, TGroup, TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser> dbContext,
			IDictionary<Type, Func<IRosterProviderService<TExternalGroup, TExternalGroupUser>>> rosterProviderServiceMapping)
		{
			_errorHandler = errorHandler ?? throw new ArgumentNullException(nameof(errorHandler));
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
			_rosterProviderServiceMapping = rosterProviderServiceMapping ?? throw new ArgumentNullException(nameof(rosterProviderServiceMapping));
		}

		public async Task SyncGroupAsync(int groupId, CancellationToken cancellationToken = default(CancellationToken))
		{
			try
			{
				var group = await _dbContext.Groups.SingleOrDefaultAsync(g => g.Id == groupId && !g.IsDeleted, cancellationToken);
				if (group == null)
					throw new EntityNotFoundException(string.Format(Strings.EntityNotFoundById, typeof(TGroup).Name, groupId));

				var externalGroups = await _dbContext.ExternalGroups.Where(gur => gur.GroupId.Equals(groupId))
					.ToListAsync(cancellationToken);
				var existingGroupUserRoles = await _dbContext.GroupUserRoles
					.Where(gur => gur.GroupId.Equals(groupId))
					.ToListAsync(cancellationToken);
				var existingExternalGroupUsers = await _dbContext.ExternalGroupUsers
					.Where(egu => egu.ExternalGroup.GroupId.Equals(groupId))
					.ToListAsync(cancellationToken);

				// removed all ExternalGroups, delete existing ExternalUserRoles and external GroupUserRoles, if any
				if (!externalGroups.Any())
				{
					var groupUserRolesToDelete = existingGroupUserRoles.Where(gur => gur.IsExternal).ToList();
					var groupUserRoleLogs = groupUserRolesToDelete
						.Select(gur => new TGroupUserRoleLog
						{
							GroupId = gur.GroupId,
							UserId = gur.UserId,
							RoleId = gur.RoleId,
							Type = GroupUserRoleLogType.Removed
						})
						.ToList();
					_dbContext.GroupUserRoleLogs.AddRange(groupUserRoleLogs);
					_dbContext.GroupUserRoles.RemoveRange(groupUserRolesToDelete);
					_dbContext.ExternalGroupUsers.RemoveRange(existingExternalGroupUsers);
					await _dbContext.SaveChangesAsync(cancellationToken);
					return;
				}

				// get rosters from all ExternalGroups
				var fullRoster = new List<ExternalRosterEntry<TExternalGroup, TExternalGroupUser>>();
				foreach (var externalGroup in externalGroups)
				{
					var externalProvider = externalGroup.ExternalProvider;
					var rosterProviderService = _rosterProviderServiceMapping[externalProvider.GetType()]();
					var roster = await rosterProviderService.GetRosterAsync(externalProvider, externalGroup, cancellationToken);
					fullRoster.AddRange(roster);
				}

				// filter distinct users by email
				var fullRosterUsers = fullRoster
					.Select(r => r.ExternalGroupUser.User)
					.GroupBy(u => u.Email)
					.SelectMany(g => g.Take(1))
					.Cast<TUser>()
					.ToList();
				// create users in db, or get existing
				var savedUsers = await _dbContext.GetOrCreateUsersAsync(fullRosterUsers, cancellationToken);

				// load all roles, to map names to ids
				var roles = await _dbContext.Roles.ToListAsync(cancellationToken);

				// construct new data from roster
				var newGroupUserRoles = fullRoster
					.Where(externalRosterEntry => externalRosterEntry.GroupUserRoles != null)
					// group by user
					.GroupBy(externalRosterEntry => externalRosterEntry.ExternalGroupUser.User.Email)
					.SelectMany(userGroup =>
							// find user's distinct roles for the group to avoid duplicates
							userGroup
								.SelectMany(externalRosterEntry => externalRosterEntry.GroupUserRoles)
								.Distinct()
								.Select(n => new TGroupUserRole
								{
									GroupId = groupId,
									UserId = savedUsers.Single(u => u.Email.Equals(userGroup.Key)).Id,
									RoleId = roles.Single(r => r.Name.Equals(n)).Id,
									IsExternal = true
								}))
					.ToList();
				var newExternalGroupUsers = fullRoster
					.Select(externalRosterEntry =>
					{
						// reference loaded User
						var userId = savedUsers.Single(u => u.Email.Equals(externalRosterEntry.ExternalGroupUser.User.Email)).Id;
						var externalGroupUser = externalRosterEntry.ExternalGroupUser;
						externalGroupUser.UserId = userId;
						externalGroupUser.User = null;
						return externalGroupUser;
					})
					.ToList();

				// add new GroupUserRoles
				var groupUserRolesToAdd = newGroupUserRoles
					.Where(gur => !existingGroupUserRoles.Any(egur =>
						egur.UserId == gur.UserId &&
						egur.RoleId == gur.RoleId))
					.ToList();
				var groupUserRolesToAddLogs = groupUserRolesToAdd
					.Select(gur => new TGroupUserRoleLog
					{
						GroupId = gur.GroupId,
						UserId = gur.UserId,
						RoleId = gur.RoleId,
						Type = GroupUserRoleLogType.Added
					})
					.ToList();
				_dbContext.GroupUserRoles.AddRange(groupUserRolesToAdd);

				// add new ExternalGroupUsers
				var externalGroupUsersToAdd = newExternalGroupUsers
					.Where(egu => !existingExternalGroupUsers.Any(eegu =>
						eegu.UserId == egu.UserId &&
						eegu.ExternalGroupId == egu.ExternalGroupId))
					.ToList();
				_dbContext.ExternalGroupUsers.AddRange(externalGroupUsersToAdd);

				// update existing local GroupUserRoles to be tracked as external if in new roster
				var groupUserRolesToUpdate = existingGroupUserRoles
					.Where(gur =>
						!gur.IsExternal &&
						newGroupUserRoles.Any(egur => egur.UserId == gur.UserId && egur.RoleId == gur.RoleId))
					.ToList();
				foreach (var groupUserRole in groupUserRolesToUpdate)
				{
					groupUserRole.IsExternal = true;
				}

				// remove existing external GroupUserRoles if not in new roster
				var groupUserRolesToRemove = existingGroupUserRoles
					.Where(gur =>
						gur.IsExternal &&
						!newGroupUserRoles.Any(egur => egur.UserId == gur.UserId && egur.RoleId == gur.RoleId))
					.ToList();
				var groupUserRolesToRemoveLogs = groupUserRolesToRemove
					.Select(gur => new TGroupUserRoleLog
					{
						GroupId = gur.GroupId,
						UserId = gur.UserId,
						RoleId = gur.RoleId,
						Type = GroupUserRoleLogType.Removed
					})
					.ToList();

				_dbContext.GroupUserRoles.RemoveRange(groupUserRolesToRemove);

				// remove existing ExternalGroupUsers if not in new roster
				var externalGroupUsersToRemove = existingExternalGroupUsers
					.Where(egu =>
						!newExternalGroupUsers.Any(eegu => eegu.UserId == egu.UserId && eegu.ExternalGroupId == egu.ExternalGroupId))
					.ToList();
				_dbContext.ExternalGroupUsers.RemoveRange(externalGroupUsersToRemove);

				// add logs
				_dbContext.GroupUserRoleLogs.AddRange(groupUserRolesToRemoveLogs);
				_dbContext.GroupUserRoleLogs.AddRange(groupUserRolesToAddLogs);

				await _dbContext.SaveChangesAsync(cancellationToken);
			}
			catch (Exception e)
			{
				var message = string.Format(Strings.RosterSyncGroupFailed, groupId);
				_logger.Error(message);
				throw new Exception(message, e);
			}
		}

		public async Task SyncAllGroupsAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var groupIds = await _dbContext.Groups
				.Where(g => !g.IsDeleted &&
							(g.ExternalGroups.Any(eg => eg.ExternalProvider.RosterSyncEnabled) ||
							g.GroupUserRoles.Any(gur => gur.IsExternal)))
				.Select(g => g.Id)
				.ToListAsync(cancellationToken);
			var successGroupIds = new List<int>();
			var failedGroupIds = new List<int>();
			foreach (var groupId in groupIds)
			{
				try
				{
					await SyncGroupAsync(groupId, cancellationToken);
					successGroupIds.Add(groupId);
				}
				catch (Exception e)
				{
					_errorHandler.CaptureException(e);
					failedGroupIds.Add(groupId);
				}
			}
			if (failedGroupIds.Any())
			{
				var message = string.Format(
					Strings.RosterSyncAllGroupsFailedAndFinished,
					string.Join(", ", failedGroupIds),
					string.Join(", ", successGroupIds));
				_logger.Error(message);
				throw new Exception(message);
			}
		}
	}
}