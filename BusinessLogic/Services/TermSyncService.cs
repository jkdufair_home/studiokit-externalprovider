﻿using StudioKit.Diagnostics;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.BusinessLogic.Services
{
	public class TermSyncService<TExternalTerm> : ITermSyncService
		where TExternalTerm : class, IExternalTerm, new()
	{
		private readonly ILogger _logger;
		private readonly ITermSyncDbContext<TExternalTerm> _dbContext;
		private readonly IDictionary<Type, Func<ITermProviderService<TExternalTerm>>> _termProviderServiceMapping;

		public TermSyncService(
			ILogger logger,
			ITermSyncDbContext<TExternalTerm> dbContext,
			IDictionary<Type, Func<ITermProviderService<TExternalTerm>>> termProviderServiceMapping)
		{
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
			_termProviderServiceMapping = termProviderServiceMapping ??
										throw new ArgumentNullException(nameof(termProviderServiceMapping));
		}

		public async Task SyncTermsAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			var externalProviders = await _dbContext.ExternalProviders.Where(p => p.TermSyncEnabled)
				.ToListAsync(cancellationToken);
			if (!externalProviders.Any())
				return;

			var existingExternalTerms = await _dbContext.ExternalTerms.ToListAsync(cancellationToken);

			var allExternalTerms = new List<TExternalTerm>();
			foreach (var externalProvider in externalProviders)
			{
				var termProviderService = _termProviderServiceMapping[externalProvider.GetType()]();
				var externalTerms = await termProviderService.GetTermsAsync(externalProvider, cancellationToken);
				allExternalTerms.AddRange(externalTerms);
			}

			_logger.Debug($"Loaded {allExternalTerms.Count} ExternalTerms");

			var externalTermsToAdd = allExternalTerms
				.Where(et => !existingExternalTerms.Any(eet =>
					et.ExternalProviderId == eet.ExternalProviderId &&
					et.ExternalId == eet.ExternalId))
				.ToList();

			_logger.Debug($"Adding {externalTermsToAdd.Count} ExternalTerms");

			_dbContext.ExternalTerms.AddRange(externalTermsToAdd);

			await _dbContext.SaveChangesAsync(cancellationToken);
		}
	}
}