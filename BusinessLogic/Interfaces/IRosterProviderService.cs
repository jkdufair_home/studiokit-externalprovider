﻿using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.BusinessLogic.Interfaces
{
	public interface IRosterProviderService<TExternalGroup, TExternalGroupUser>
		where TExternalGroup : class, IExternalGroup
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>
	{
		Task<IEnumerable<ExternalRosterEntry<TExternalGroup, TExternalGroupUser>>> GetRosterAsync(
			ExternalProvider.Models.ExternalProvider externalProvider,
			TExternalGroup externalGroup, CancellationToken cancellationToken = default(CancellationToken));
	}
}