﻿using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.BusinessLogic.Interfaces
{
	public interface IRosterSyncService
	{
		Task SyncGroupAsync(int groupId, CancellationToken cancellationToken = default(CancellationToken));

		Task SyncAllGroupsAsync(CancellationToken cancellationToken = default(CancellationToken));
	}
}