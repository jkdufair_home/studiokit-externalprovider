﻿using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.BusinessLogic.Models
{
	public class ExternalRosterEntry<TExternalGroup, TExternalGroupUser>
		where TExternalGroup : class, IExternalGroup
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>
	{
		public TExternalGroupUser ExternalGroupUser { get; set; }

		public IEnumerable<string> GroupUserRoles { get; set; }
	}
}