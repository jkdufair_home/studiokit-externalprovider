﻿using StudioKit.Data;
using System.ComponentModel.DataAnnotations;

namespace StudioKit.ExternalProvider.Models
{
	public abstract class ExternalProvider : ModelBase
	{
		[Required]
		public string Name { get; set; }

		public bool TermSyncEnabled { get; set; }

		public bool RosterSyncEnabled { get; set; }

		public bool GradePushEnabled { get; set; }
	}
}