﻿using System;

namespace StudioKit.ExternalProvider.Models.Interfaces
{
	public interface IGroupAssignmentScore
	{
		string CreatedById { get; set; }

		decimal? TotalPoints { get; set; }

		DateTime DateLastUpdated { get; set; }
	}
}