﻿namespace StudioKit.ExternalProvider.Models.Interfaces
{
	public interface IAssignment
	{
		int Id { get; set; }

		decimal? Points { get; set; }

		string Name { get; set; }

		bool IsDeleted { get; set; }
	}
}