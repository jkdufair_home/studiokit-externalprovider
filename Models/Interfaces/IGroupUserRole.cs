﻿namespace StudioKit.ExternalProvider.Models.Interfaces
{
	public interface IGroupUserRole
	{
		int GroupId { get; set; }

		string UserId { get; set; }

		string RoleId { get; set; }

		bool IsExternal { get; set; }
	}
}