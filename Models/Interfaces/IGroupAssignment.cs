﻿namespace StudioKit.ExternalProvider.Models.Interfaces
{
	public interface IGroupAssignment
	{
		int Id { get; set; }

		int GroupId { get; set; }

		int AssignmentId { get; set; }

		bool IsDeleted { get; set; }
	}
}