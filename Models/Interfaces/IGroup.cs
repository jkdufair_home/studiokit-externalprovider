﻿using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Models.Interfaces
{
	public interface IGroup<TGroupUserRole, TExternalGroup>
		where TGroupUserRole : IGroupUserRole
		where TExternalGroup : IExternalGroup
	{
		int Id { get; set; }

		bool IsDeleted { get; set; }

		ICollection<TGroupUserRole> GroupUserRoles { get; set; }

		ICollection<TExternalGroup> ExternalGroups { get; set; }
	}
}