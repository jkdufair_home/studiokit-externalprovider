﻿namespace StudioKit.ExternalProvider.Models.Interfaces
{
	public interface IExternalGroupAssignment
	{
		int Id { get; set; }

		int ExternalGroupId { get; set; }

		int GroupAssignmentId { get; set; }

		string ExternalId { get; set; }
	}
}