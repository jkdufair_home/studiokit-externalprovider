﻿using StudioKit.Data.Entity.Identity.Interfaces;

namespace StudioKit.ExternalProvider.Models.Interfaces
{
	public interface IExternalGroup
	{
		int Id { get; set; }

		int GroupId { get; set; }

		string ExternalId { get; set; }

		int ExternalProviderId { get; set; }

		ExternalProvider ExternalProvider { get; set; }

		string UserId { get; set; }

		IUser User { get; set; }

		string Name { get; set; }

		string Description { get; set; }

		string RosterUrl { get; set; }

		string GradesUrl { get; set; }

		/// <summary>
		/// Whether the system should automatically push grades, or a user will manually trigger the push. Should be `null` if the <see cref="ExternalProvider"/> does not allow grade push (<see cref="ExternalProvider.GradePushEnabled"/>).
		/// </summary>
		bool? IsAutoGradePushEnabled { get; set; }
	}
}