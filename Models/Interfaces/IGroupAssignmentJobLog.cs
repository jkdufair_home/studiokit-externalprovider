﻿namespace StudioKit.ExternalProvider.Models.Interfaces
{
	public interface IGroupAssignmentJobLog
	{
		int GroupAssignmentId { get; set; }

		string Type { get; set; }

		string QueueMessageId { get; set; }

		string ExceptionMessage { get; set; }
	}
}