﻿namespace StudioKit.ExternalProvider.Models.Interfaces
{
	public enum GroupUserRoleLogType
	{
		Added,
		Removed
	}

	public interface IGroupUserRoleLog
	{
		int GroupId { get; set; }

		string UserId { get; set; }

		string RoleId { get; set; }

		GroupUserRoleLogType Type { get; set; }
	}
}