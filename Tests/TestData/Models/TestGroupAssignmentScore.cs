﻿using Microsoft.Build.Framework;
using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestGroupAssignmentScore : ModelBase, IGroupAssignmentScore
	{
		public string CreatedById { get; set; }

		public decimal? TotalPoints { get; set; }

		/// <summary>
		/// For easy querying in tests
		/// </summary>
		[Required]
		public int GroupAssignmentId { get; set; }

		[ForeignKey(nameof(GroupAssignmentId))]
		public virtual TestGroupAssignment GroupAssignment { get; set; }
	}
}