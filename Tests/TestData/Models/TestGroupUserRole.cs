﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestGroupUserRole : ModelBase, IGroupUserRole
	{
		[Index("IX_EntityUserRole_Entity", IsUnique = true, Order = 0)]
		[StringLength(128)]
		[Required]
		public string UserId { get; set; }

		[Index("IX_EntityUserRole_Entity", IsUnique = true, Order = 1)]
		[StringLength(128)]
		[Required]
		public string RoleId { get; set; }

		[ForeignKey("UserId")]
		public virtual IUser User { get; set; }

		[ForeignKey(nameof(RoleId))]
		public virtual Role Role { get; set; }

		[Index("IX_EntityUserRole_Entity", IsUnique = true, Order = 2)]
		public int GroupId { get; set; }

		[ForeignKey(nameof(GroupId))]
		public virtual TestGroup Group { get; set; }

		public bool IsExternal { get; set; }
	}
}