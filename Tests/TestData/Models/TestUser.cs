﻿using Microsoft.AspNet.Identity.EntityFramework;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestUser : IdentityUser<string, UserLogin, UserRole, UserClaim>, IUser
	{
		public TestUser()
		{
			Id = Guid.NewGuid().ToString();
		}

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string FullName => $"{FirstName} {LastName}";

		public string Uid { get; set; }

		public string EmployeeNumber { get; set; }

		[NotMapped]
		public string CareerAccountAlias
		{
			get => Uid;
			set => Uid = value;
		}

		[NotMapped]
		public string Puid
		{
			get => EmployeeNumber;
			set => EmployeeNumber = value;
		}

		public DateTime DateStored { get; set; }

		public DateTime DateLastUpdated { get; set; }

		public string LastUpdatedById { get; set; }
	}
}