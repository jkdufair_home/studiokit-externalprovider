﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestExternalGroup : ModelBase, IExternalGroup
	{
		[Required]
		[Index("IX_GroupId_ExternalId_ExternalProviderId", 1, IsUnique = true)]
		public int GroupId { get; set; }

		[ForeignKey(nameof(GroupId))]
		public TestGroup Group { get; set; }

		[Required]
		[StringLength(450)]
		[Index("IX_GroupId_ExternalId_ExternalProviderId", 2, IsUnique = true)]
		public string ExternalId { get; set; }

		[Required]
		[Index("IX_GroupId_ExternalId_ExternalProviderId", 3, IsUnique = true)]
		public int ExternalProviderId { get; set; }

		[ForeignKey(nameof(ExternalProviderId))]
		public ExternalProvider.Models.ExternalProvider ExternalProvider { get; set; }

		[Required]
		public string UserId { get; set; }

		[ForeignKey(nameof(UserId))]
		public virtual IUser User { get; set; }

		[Required]
		public string Name { get; set; }

		public string Description { get; set; }

		public string RosterUrl { get; set; }

		public string GradesUrl { get; set; }

		public bool? IsAutoGradePushEnabled { get; set; }

		public virtual ICollection<TestExternalGroupUser> ExternalGroupUsers { get; set; }
	}
}