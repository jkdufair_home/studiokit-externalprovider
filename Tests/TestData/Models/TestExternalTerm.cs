﻿using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestExternalTerm : ModelBase, IExternalTerm
	{
		[Required]
		[Index("IX_ExternalProviderId_ExternalId", 1)]
		public int ExternalProviderId { get; set; }

		[ForeignKey(nameof(ExternalProviderId))]
		public ExternalProvider.Models.ExternalProvider ExternalProvider { get; set; }

		[Required]
		[StringLength(450)]
		[Index("IX_ExternalProviderId_ExternalId", 2)]
		public string ExternalId { get; set; }

		public string Name { get; set; }

		[Required]
		public DateTime StartDate { get; set; }

		[Required]
		public DateTime EndDate { get; set; }
	}
}