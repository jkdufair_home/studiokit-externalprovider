﻿using StudioKit.Data;
using StudioKit.Data.Entity.Identity.Interfaces;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestGroupUserRoleLog : ModelBase, IGroupUserRoleLog
	{
		[Required]
		public int GroupId { get; set; }

		[ForeignKey(nameof(GroupId))]
		public virtual TestGroup Group { get; set; }

		[Required]
		public string UserId { get; set; }

		[ForeignKey(nameof(UserId))]
		public virtual IUser User { get; set; }

		[Required]
		public string RoleId { get; set; }

		[ForeignKey(nameof(RoleId))]
		public virtual Role Role { get; set; }

		[Required]
		public GroupUserRoleLogType Type { get; set; }
	}
}