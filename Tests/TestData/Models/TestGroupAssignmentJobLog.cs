﻿using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestGroupAssignmentJobLog : ModelBase, IGroupAssignmentJobLog
	{
		[Required]
		public int GroupAssignmentId { get; set; }

		[ForeignKey(nameof(GroupAssignmentId))]
		public virtual TestGroupAssignment GroupAssignment { get; set; }

		[Required]
		public string Type { get; set; }

		public string QueueMessageId { get; set; }

		public string ExceptionMessage { get; set; }
	}
}