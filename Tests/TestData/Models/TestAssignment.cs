﻿using StudioKit.Data;
using StudioKit.Data.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestAssignment : ModelBase, IDelible, IAssignment
	{
		[Required]
		public string Name { get; set; }

		public decimal? Points { get; set; }

		[Index("IX_Assignment_IsDeleted")]
		public bool IsDeleted { get; set; }

		public virtual ICollection<TestGroupAssignment> GroupAssignments { get; set; }
	}
}