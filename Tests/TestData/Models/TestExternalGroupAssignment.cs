﻿using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestExternalGroupAssignment : ModelBase, IExternalGroupAssignment
	{
		[Required]
		public int ExternalGroupId { get; set; }

		[ForeignKey(nameof(ExternalGroupId))]
		public virtual TestExternalGroup ExternalGroup { get; set; }

		[Required]
		public int GroupAssignmentId { get; set; }

		[ForeignKey(nameof(GroupAssignmentId))]
		public virtual TestGroupAssignment GroupAssignment { get; set; }

		[Required]
		public string ExternalId { get; set; }
	}
}