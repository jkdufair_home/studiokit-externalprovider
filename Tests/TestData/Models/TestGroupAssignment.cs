﻿using StudioKit.Data;
using StudioKit.Data.Interfaces;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestGroupAssignment : ModelBase, IDelible, IGroupAssignment
	{
		[Required]
		[Index("IX_GroupAssignment_GroupId")]
		public int GroupId { get; set; }

		[ForeignKey(nameof(GroupId))]
		public virtual TestGroup Group { get; set; }

		[Required]
		[Index("IX_GroupAssignment_AssignmentId")]
		public int AssignmentId { get; set; }

		[ForeignKey(nameof(AssignmentId))]
		public virtual TestAssignment Assignment { get; set; }

		[Index("IX_GroupAssignment_IsDeleted")]
		public bool IsDeleted { get; set; }

		public virtual ICollection<TestGroupAssignmentJobLog> Logs { get; set; }
	}
}