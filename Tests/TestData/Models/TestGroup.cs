﻿using StudioKit.Data;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Tests.TestData.Models
{
	public class TestGroup : ModelBase, IGroup<TestGroupUserRole, TestExternalGroup>
	{
		public bool IsDeleted { get; set; }

		public virtual ICollection<TestGroupUserRole> GroupUserRoles { get; set; }

		public virtual ICollection<TestGroupUserRoleLog> GroupUserRoleLogs { get; set; }

		public virtual ICollection<TestExternalGroup> ExternalGroups { get; set; }
	}
}