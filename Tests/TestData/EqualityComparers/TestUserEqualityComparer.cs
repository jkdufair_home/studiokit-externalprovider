﻿using StudioKit.Data.Entity.Identity.Interfaces;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Tests.TestData.EqualityComparers
{
	public class TestUserEqualityComparer : IEqualityComparer<IUser>
	{
		public bool Equals(IUser x, IUser y)
		{
			if (x == y) return true;
			if (x is null) return false;
			if (y is null) return false;
			if (x.GetType() != y.GetType()) return false;
			return string.Equals(x.UserName, y.UserName) &&
					string.Equals(x.Email, y.Email) &&
					string.Equals(x.FirstName, y.FirstName) &&
					string.Equals(x.LastName, y.LastName) &&
					string.Equals(x.EmployeeNumber, y.EmployeeNumber) &&
					string.Equals(x.Uid, y.Uid);
		}

		public int GetHashCode(IUser obj)
		{
			unchecked
			{
				var hashCode = obj.UserName != null ? obj.UserName.GetHashCode() : 0;
				hashCode = (hashCode * 397) ^ (obj.Email != null ? obj.Email.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.FirstName != null ? obj.FirstName.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.LastName != null ? obj.LastName.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.EmployeeNumber != null ? obj.EmployeeNumber.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.Uid != null ? obj.Uid.GetHashCode() : 0);
				return hashCode;
			}
		}
	}
}