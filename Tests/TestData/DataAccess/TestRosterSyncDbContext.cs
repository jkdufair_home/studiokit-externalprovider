﻿using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Tests.TestData.DataAccess
{
	public class TestRosterSyncDbContext : UserIdentityDbContext<TestUser, IdentityProvider>,
		IRosterSyncDbContext<TestUser, TestGroup, TestGroupUserRole, TestGroupUserRoleLog, TestExternalGroup, TestExternalGroupUser>
	{
		public TestRosterSyncDbContext()
		{
		}

		public TestRosterSyncDbContext(DbConnection existingConnection) : base(existingConnection)
		{
		}

		public DbSet<ExternalProvider.Models.ExternalProvider> ExternalProviders { get; set; }

		public DbSet<TestExternalGroup> ExternalGroups { get; set; }

		public DbSet<TestExternalGroupUser> ExternalGroupUsers { get; set; }

		public DbSet<TestGroup> Groups { get; set; }

		public DbSet<TestGroupUserRole> GroupUserRoles { get; set; }

		public DbSet<TestGroupUserRoleLog> GroupUserRoleLogs { get; set; }

		public async Task<IEnumerable<TestUser>> GetOrCreateUsersAsync(IEnumerable<TestUser> usersEnumerable, CancellationToken cancellationToken = default(CancellationToken))
		{
			var users = usersEnumerable.ToList();
			var usernames = users.Select(u => u.UserName).ToList();
			// Note: not updating user details
			var existingUsers = await Users.Where(u => usernames.Contains(u.UserName)).ToListAsync(cancellationToken);
			var usersToAdd = users.Where(ul => !existingUsers.Any(e => e.UserName.Equals(ul.UserName))).ToList();
			((DbSet<TestUser>)Users).AddRange(usersToAdd);
			await SaveChangesAsync(cancellationToken);
			return new List<TestUser>(existingUsers.Concat(usersToAdd));
		}
	}
}