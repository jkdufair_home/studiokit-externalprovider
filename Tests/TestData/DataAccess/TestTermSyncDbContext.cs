﻿using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.DataAccess.Interfaces;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Data.Common;
using System.Data.Entity;

namespace StudioKit.ExternalProvider.Tests.TestData.DataAccess
{
	public class TestTermSyncDbContext : UserIdentityDbContext<TestUser, IdentityProvider>,
		ITermSyncDbContext<TestExternalTerm>
	{
		public TestTermSyncDbContext()
		{
		}

		public TestTermSyncDbContext(DbConnection existingConnection) : base(existingConnection)
		{
		}

		public DbSet<ExternalProvider.Models.ExternalProvider> ExternalProviders { get; set; }

		public DbSet<TestExternalTerm> ExternalTerms { get; set; }
	}
}