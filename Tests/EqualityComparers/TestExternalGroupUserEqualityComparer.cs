﻿using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Tests.EqualityComparers
{
	public class TestExternalGroupUserEqualityComparer : IEqualityComparer<TestExternalGroupUser>
	{
		public bool Equals(TestExternalGroupUser x, TestExternalGroupUser y)
		{
			if (ReferenceEquals(x, y)) return true;
			if (x is null) return false;
			if (y is null) return false;
			if (x.GetType() != y.GetType()) return false;
			return x.ExternalGroupId == y.ExternalGroupId && string.Equals(x.UserId, y.UserId) && string.Equals(x.ExternalUserId, y.ExternalUserId) && string.Equals(x.Roles, y.Roles);
		}

		public int GetHashCode(TestExternalGroupUser obj)
		{
			unchecked
			{
				var hashCode = obj.ExternalGroupId;
				hashCode = (hashCode * 397) ^ (obj.UserId != null ? obj.UserId.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.ExternalUserId != null ? obj.ExternalUserId.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.Roles != null ? obj.Roles.GetHashCode() : 0);
				return hashCode;
			}
		}
	}
}