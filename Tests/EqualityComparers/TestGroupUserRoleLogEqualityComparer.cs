﻿using StudioKit.ExternalProvider.Tests.TestData.Models;
using System.Collections.Generic;

namespace StudioKit.ExternalProvider.Tests.EqualityComparers
{
	public class TestGroupUserRoleLogEqualityComparer : IEqualityComparer<TestGroupUserRoleLog>
	{
		public bool Equals(TestGroupUserRoleLog x, TestGroupUserRoleLog y)
		{
			if (ReferenceEquals(x, y)) return true;
			if (ReferenceEquals(x, null)) return false;
			if (ReferenceEquals(y, null)) return false;
			if (x.GetType() != y.GetType()) return false;
			return x.GroupId == y.GroupId && string.Equals(x.UserId, y.UserId) && string.Equals(x.RoleId, y.RoleId) && x.Type == y.Type;
		}

		public int GetHashCode(TestGroupUserRoleLog obj)
		{
			unchecked
			{
				var hashCode = obj.GroupId;
				hashCode = (hashCode * 397) ^ (obj.UserId != null ? obj.UserId.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (obj.RoleId != null ? obj.RoleId.GetHashCode() : 0);
				hashCode = (hashCode * 397) ^ (int)obj.Type;
				return hashCode;
			}
		}
	}
}