using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StudioKit.ExternalProvider.Lti.Tests
{
	public class DbProviderSetup
	{
		[AssemblyInitialize]
		public void RegisterEffortProvider()
		{
			Effort.Provider.EffortProviderConfiguration.RegisterProvider();
		}
	}
}