﻿using Effort;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Data.Entity.Identity;
using StudioKit.Data.Entity.Identity.Models;
using StudioKit.Diagnostics;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Models;
using StudioKit.ExternalProvider.BusinessLogic.Services;
using StudioKit.ExternalProvider.Models.Interfaces;
using StudioKit.ExternalProvider.Properties;
using StudioKit.ExternalProvider.Tests.EqualityComparers;
using StudioKit.ExternalProvider.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Tests.BusinessLogic.Services
{
	[TestClass]
	public class RosterSyncServiceTests : BaseTest
	{
		private ILogger _logger;
		private IErrorHandler _errorHandler;
		private IDictionary<Type, Func<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>> _rosterProviderServiceMapping;

		[TestInitialize]
		public void Initialize()
		{
			_logger = new Mock<ILogger>().Object;
			_errorHandler = new Mock<IErrorHandler>().Object;
			_rosterProviderServiceMapping = new Dictionary<Type, Func<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>>();
		}

		#region SyncGroupAsync

		[TestMethod]
		public void SyncGroupAsync_ShouldThrowIfGroupNotFound()
		{
			using (var dbContext = new TestRosterSyncDbContext(DbConnectionFactory.CreateTransient()))
			{
				var service = GetService(dbContext);

				Assert.ThrowsAsync<Exception>(
					Task.Run(async () =>
					{
						await service.SyncGroupAsync(1);
					}), string.Format(Strings.RosterSyncGroupFailed, 1));
			}
		}

		[TestMethod]
		public async Task SyncGroupAsync_ShouldAddCrossListedStudentEnrollments()
		{
			using (var dbContext = new TestRosterSyncDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedAsync(dbContext);

				var externalProvider = new TestExternalProvider
				{
					Name = "Test",
					RosterSyncEnabled = true,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.ExternalProviders.Add(externalProvider);
				dbContext.Users.Add(new TestUser { Id = "1", UserName = "1" });
				await dbContext.SaveChangesAsync();

				var group = new TestGroup
				{
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.Groups.Add(group);
				await dbContext.SaveChangesAsync();

				var externalGroups = new List<TestExternalGroup>
				{
					new TestExternalGroup
					{
						GroupId = group.Id,
						ExternalId = "1234",
						UserId = "1",
						Name = "Course 1",
						ExternalProviderId = externalProvider.Id,
						DateStored = DateTime.UtcNow,
						DateLastUpdated = DateTime.UtcNow
					},
					new TestExternalGroup
					{
						GroupId = group.Id,
						ExternalId = "5678",
						UserId = "1",
						Name = "Course 2",
						ExternalProviderId = externalProvider.Id,
						DateStored = DateTime.UtcNow,
						DateLastUpdated = DateTime.UtcNow
					}
				};
				dbContext.ExternalGroups.AddRange(externalGroups);
				await dbContext.SaveChangesAsync();

				var course1Roster = new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
				{
					new ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>
					{
						ExternalGroupUser = new TestExternalGroupUser
						{
							ExternalGroupId = externalGroups.First().Id,
							ExternalUserId = "0000000000",
							User = new TestUser
							{
								UserName = "user1@external.org",
								Email = "user1@external.org",
								FirstName = "FirstName 1",
								LastName = "LastName",
								EmployeeNumber = "0000000000",
								Uid = "user1"
							}
						},
						GroupUserRoles = new List<string>
						{
							BaseRole.GroupLearner
						}
					},
					new ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>
					{
						ExternalGroupUser = new TestExternalGroupUser
						{
							ExternalGroupId = externalGroups.First().Id,
							ExternalUserId = "0000000001",
							User = new TestUser
							{
								UserName = "user2@external.org",
								Email = "user2@external.org",
								FirstName = "FirstName 2",
								LastName = "LastName",
								EmployeeNumber = "0000000001",
								Uid = "user2"
							}
						},
						GroupUserRoles = new List<string>
						{
							BaseRole.GroupLearner
						}
					}
				};
				var course2Roster = new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
				{
					new ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>
					{
						ExternalGroupUser = new TestExternalGroupUser
						{
							ExternalGroupId = externalGroups.Skip(1).First().Id,
							ExternalUserId = "0000000000",
							User = new TestUser
							{
								UserName = "user1@external.org",
								Email = "user1@external.org",
								FirstName = "FirstName 1",
								LastName = "LastName",
								EmployeeNumber = "0000000000",
								Uid = "user1"
							}
						},
						GroupUserRoles = new List<string>
						{
							BaseRole.GroupLearner
						}
					}
				};

				var rosterProviderServiceMapping =
					new Dictionary<Type, Func<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>>
					{
						{typeof(TestExternalProvider), () =>
						{
							var mockRosterProviderService = new Mock<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>();

							mockRosterProviderService
								.Setup(x => x.GetRosterAsync(
									It.IsAny<Models.ExternalProvider>(),
									It.Is<TestExternalGroup>(eg => eg.Id == externalGroups.First().Id),
									It.IsAny<CancellationToken>()))
								.ReturnsAsync(course1Roster);

							mockRosterProviderService
								.Setup(x => x.GetRosterAsync(
									It.IsAny<Models.ExternalProvider>(),
									It.Is<TestExternalGroup>(eg => eg.Id == externalGroups.Skip(1).First().Id),
									It.IsAny<CancellationToken>()))
								.ReturnsAsync(course2Roster);

							return mockRosterProviderService.Object;
						}}
					};

				var service = GetService(dbContext, rosterProviderServiceMapping);
				await service.SyncGroupAsync(group.Id);

				var groupUserRoles = await dbContext.GroupUserRoles.Where(gur => gur.GroupId.Equals(group.Id)).ToListAsync();
				var groupUserRoleLogs = await dbContext.GroupUserRoleLogs.Where(gur => gur.GroupId.Equals(group.Id)).ToListAsync();
				var externalGroupUsers = await dbContext.ExternalGroupUsers.Where(egu => egu.ExternalGroup.GroupId.Equals(group.Id)).ToListAsync();

				Assert.AreEqual(2, groupUserRoles.Count, "Distinct number of GroupUserRoles");
				Assert.AreEqual(2, groupUserRoleLogs.Count, "Distinct number of GroupUserRoleLogs");
				Assert.AreEqual(3, externalGroupUsers.Count, "Total number of ExternalGroupUsers, from all cross-listed enrollments");
			}
		}

		[TestMethod]
		public async Task SyncGroupAsync_ShouldAddUpdateAndRemoveEnrollments()
		{
			using (var dbContext = new TestRosterSyncDbContext(DbConnectionFactory.CreateTransient()))
			{
				await SeedAsync(dbContext);

				var roles = await dbContext.Roles.ToListAsync();
				var groupOwnerRoleId = roles.Single(r => r.Name.Equals(BaseRole.GroupOwner)).Id;
				var groupLearnerRoleId = roles.Single(r => r.Name.Equals(BaseRole.GroupLearner)).Id;

				var externalProvider = new TestExternalProvider
				{
					Name = "Test",
					RosterSyncEnabled = true,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.ExternalProviders.Add(externalProvider);
				dbContext.Users.Add(new TestUser
				{
					Id = "1",
					UserName = "1",
					Email = "1@external.org",
					FirstName = "FirstName 1",
					LastName = "LastName",
					EmployeeNumber = "0000000001",
					Uid = "1"
				});
				dbContext.Users.Add(new TestUser
				{
					Id = "2",
					UserName = "2",
					Email = "2@external.org",
					FirstName = "FirstName 2",
					LastName = "LastName",
					EmployeeNumber = "0000000002",
					Uid = "2"
				});
				dbContext.Users.Add(new TestUser
				{
					Id = "3",
					UserName = "3",
					Email = "3@external.org",
					FirstName = "FirstName 3",
					LastName = "LastName",
					EmployeeNumber = "0000000003",
					Uid = "3"
				});
				dbContext.Users.Add(new TestUser
				{
					Id = "4",
					UserName = "4",
					Email = "4@external.org",
					FirstName = "FirstName 4",
					LastName = "LastName",
					EmployeeNumber = "0000000004",
					Uid = "4"
				});
				await dbContext.SaveChangesAsync();

				var group = new TestGroup
				{
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow,
					GroupUserRoles = new List<TestGroupUserRole>
					{
						new TestGroupUserRole
						{
							UserId = "1",
							RoleId = groupOwnerRoleId,
							IsExternal = false, // Note: instructor is NOT external, e.g. UniTime TODO: test external intructor, e.g. Lti
							DateStored = DateTime.UtcNow,
							DateLastUpdated = DateTime.UtcNow
						},
						new TestGroupUserRole
						{
							UserId = "2",
							RoleId = groupLearnerRoleId,
							IsExternal = true,
							DateStored = DateTime.UtcNow,
							DateLastUpdated = DateTime.UtcNow
						},
						new TestGroupUserRole
						{
							UserId = "3",
							RoleId = groupLearnerRoleId,
							IsExternal = false,
							DateStored = DateTime.UtcNow,
							DateLastUpdated = DateTime.UtcNow
						}
					},
					GroupUserRoleLogs = new List<TestGroupUserRoleLog>
					{
						new TestGroupUserRoleLog
						{
							UserId = "1",
							RoleId = groupOwnerRoleId,
							Type = GroupUserRoleLogType.Added,
							DateStored = DateTime.UtcNow,
							DateLastUpdated = DateTime.UtcNow
						},
						new TestGroupUserRoleLog
						{
							UserId = "2",
							RoleId = groupLearnerRoleId,
							Type = GroupUserRoleLogType.Added,
							DateStored = DateTime.UtcNow,
							DateLastUpdated = DateTime.UtcNow
						},
						new TestGroupUserRoleLog
						{
							UserId = "3",
							RoleId = groupLearnerRoleId,
							Type = GroupUserRoleLogType.Added,
							DateStored = DateTime.UtcNow,
							DateLastUpdated = DateTime.UtcNow
						}
					}
				};
				dbContext.Groups.Add(group);
				await dbContext.SaveChangesAsync();

				var externalGroup = new TestExternalGroup
				{
					GroupId = group.Id,
					ExternalId = "1234",
					UserId = "1",
					Name = "Course 1",
					ExternalProviderId = externalProvider.Id,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.ExternalGroupUsers.AddRange(new List<TestExternalGroupUser>
				{
					new TestExternalGroupUser
					{
						UserId = "2",
						ExternalUserId = "b",
						ExternalGroup = externalGroup,
						DateStored = DateTime.UtcNow,
						DateLastUpdated = DateTime.UtcNow
					}
				});
				dbContext.ExternalGroups.Add(externalGroup);
				await dbContext.SaveChangesAsync();

				var roster = new List<ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>>
				{
					new ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>
					{
						ExternalGroupUser = new TestExternalGroupUser
						{
							ExternalGroupId = externalGroup.Id,
							ExternalUserId = "c",
							User = new TestUser
							{
								UserName = "3",
								Email = "3@external.org",
								FirstName = "FirstName 3",
								LastName = "LastName",
								EmployeeNumber = "0000000003",
								Uid = "3"
							}
						},
						GroupUserRoles = new List<string>
						{
							BaseRole.GroupLearner
						}
					},
					new ExternalRosterEntry<TestExternalGroup, TestExternalGroupUser>
					{
						ExternalGroupUser = new TestExternalGroupUser
						{
							ExternalGroupId = externalGroup.Id,
							ExternalUserId = "d",
							User = new TestUser
							{
								UserName = "4",
								Email = "4@external.org",
								FirstName = "FirstName 4",
								LastName = "LastName",
								EmployeeNumber = "0000000004",
								Uid = "4"
							}
						},
						GroupUserRoles = new List<string>
						{
							BaseRole.GroupLearner
						}
					}
				};

				var rosterProviderServiceMapping =
					new Dictionary<Type, Func<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>>
					{
						{typeof(TestExternalProvider), () =>
						{
							var mockRosterProviderService = new Mock<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>();

							mockRosterProviderService
								.Setup(x => x.GetRosterAsync(
									It.IsAny<Models.ExternalProvider>(),
									It.IsAny<TestExternalGroup>(),
									It.IsAny<CancellationToken>()))
								.ReturnsAsync(roster);

							return mockRosterProviderService.Object;
						}}
					};

				var service = GetService(dbContext, rosterProviderServiceMapping);
				await service.SyncGroupAsync(group.Id);

				var groupUserRoles = await dbContext.GroupUserRoles.Where(gur => gur.GroupId.Equals(group.Id)).ToListAsync();
				var groupUserRoleLogs = await dbContext.GroupUserRoleLogs.Where(gur => gur.GroupId.Equals(group.Id)).ToListAsync();
				var externalGroupUsers = await dbContext.ExternalGroupUsers.Where(egu => egu.ExternalGroup.GroupId.Equals(group.Id)).ToListAsync();

				Assert.IsTrue(groupUserRoles.OrderBy(gur => gur.UserId).SequenceEqual(new List<TestGroupUserRole>
				{
					new TestGroupUserRole
					{
						GroupId = group.Id,
						UserId = "1",
						RoleId = groupOwnerRoleId,
						IsExternal = false
					},
					new TestGroupUserRole
					{
						GroupId = group.Id,
						UserId = "3",
						RoleId = groupLearnerRoleId,
						IsExternal = true
					},
					new TestGroupUserRole
					{
						GroupId = group.Id,
						UserId = "4",
						RoleId = groupLearnerRoleId,
						IsExternal = true
					}
				}, new TestGroupUserRoleEqualityComparer()), "removed user 2, updated user 3 to IsExternal, added user 4");

				Assert.IsTrue(groupUserRoleLogs.SequenceEqual(new List<TestGroupUserRoleLog>
				{
					new TestGroupUserRoleLog
					{
						GroupId = group.Id,
						UserId = "1",
						RoleId = groupOwnerRoleId,
						Type = GroupUserRoleLogType.Added
					},
					new TestGroupUserRoleLog
					{
						GroupId = group.Id,
						UserId = "2",
						RoleId = groupLearnerRoleId,
						Type = GroupUserRoleLogType.Added
					},
					new TestGroupUserRoleLog
					{
						GroupId = group.Id,
						UserId = "3",
						RoleId = groupLearnerRoleId,
						Type = GroupUserRoleLogType.Added
					},
					new TestGroupUserRoleLog
					{
						GroupId = group.Id,
						UserId = "2",
						RoleId = groupLearnerRoleId,
						Type = GroupUserRoleLogType.Removed
					},
					new TestGroupUserRoleLog
					{
						GroupId = group.Id,
						UserId = "4",
						RoleId = groupLearnerRoleId,
						Type = GroupUserRoleLogType.Added
					}
				}, new TestGroupUserRoleLogEqualityComparer()), "removed user 2, added user 4");

				Assert.IsTrue(externalGroupUsers.SequenceEqual(new List<TestExternalGroupUser>
				{
					new TestExternalGroupUser
					{
						UserId = "3",
						ExternalUserId = "c",
						ExternalGroupId = externalGroup.Id
					},
					new TestExternalGroupUser
					{
						UserId = "4",
						ExternalUserId = "d",
						ExternalGroupId = externalGroup.Id
					}
				}, new TestExternalGroupUserEqualityComparer()), "removed user 2, added user 3, added user 4");
			}
		}

		#endregion SyncGroupAsync

		#region Helpers

		private IRosterSyncService GetService(
			TestRosterSyncDbContext dbContext,
			IDictionary<Type, Func<IRosterProviderService<TestExternalGroup, TestExternalGroupUser>>> rosterProviderServiceMapping = null)
		{
			return new RosterSyncService<TestUser, TestGroup, TestGroupUserRole, TestGroupUserRoleLog, TestExternalGroup,
				TestExternalGroupUser>(_errorHandler, _logger, dbContext, rosterProviderServiceMapping ?? _rosterProviderServiceMapping);
		}

		private static async Task SeedAsync(TestRosterSyncDbContext dbContext, CancellationToken cancellationToken = default(CancellationToken))
		{
			((DbSet<Role>)dbContext.Roles).AddRange(new List<Role>
			{
				new Role(BaseRole.SuperAdmin),
				new Role(BaseRole.Admin),
				new Role(BaseRole.Creator),
				new Role(BaseRole.GroupOwner),
				new Role(BaseRole.GroupLearner)
			});
			await dbContext.SaveChangesAsync(cancellationToken);
		}

		#endregion Helpers
	}
}