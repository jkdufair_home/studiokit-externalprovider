﻿using Effort;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using MSTestExtensions;
using StudioKit.Diagnostics;
using StudioKit.ExternalProvider.BusinessLogic.Interfaces;
using StudioKit.ExternalProvider.BusinessLogic.Services;
using StudioKit.ExternalProvider.Tests.TestData.DataAccess;
using StudioKit.ExternalProvider.Tests.TestData.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.Tests.BusinessLogic.Services
{
	[TestClass]
	public class TermSyncServiceTests : BaseTest
	{
		private ILogger _logger;
		private IDictionary<Type, Func<ITermProviderService<TestExternalTerm>>> _termProviderServiceMapping;

		[TestInitialize]
		public void Initialize()
		{
			_logger = new Mock<ILogger>().Object;
			_termProviderServiceMapping = new Dictionary<Type, Func<ITermProviderService<TestExternalTerm>>>();
		}

		#region SyncTermsAsync

		[TestMethod]
		public async Task SyncTermsAsync_ShouldReturnIfNoTermSyncEnabledExternalProviders()
		{
			using (var dbContext = new TestTermSyncDbContext(DbConnectionFactory.CreateTransient()))
			{
				var service = GetService(dbContext);
				await service.SyncTermsAsync();
				var externalTermCount = await dbContext.ExternalTerms.CountAsync();
				Assert.AreEqual(0, externalTermCount);
			}
		}

		[TestMethod]
		public async Task SyncTermsAsync_ShouldAddNewExternalTermsAndPreserveExisting()
		{
			using (var dbContext = new TestTermSyncDbContext(DbConnectionFactory.CreateTransient()))
			{
				var externalProvider = new TestExternalProvider
				{
					Name = "Test",
					TermSyncEnabled = true,
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.ExternalProviders.Add(externalProvider);
				await dbContext.SaveChangesAsync();

				var existingExternalTerm = new TestExternalTerm
				{
					ExternalProviderId = externalProvider.Id,
					ExternalId = "abc",
					Name = "Existing Term",
					StartDate = DateTime.UtcNow.AddDays(-30),
					EndDate = DateTime.UtcNow.AddDays(30),
					DateStored = DateTime.UtcNow,
					DateLastUpdated = DateTime.UtcNow
				};
				dbContext.ExternalTerms.Add(existingExternalTerm);
				await dbContext.SaveChangesAsync();

				var providedExternalTerms = new List<TestExternalTerm>
				{
					new TestExternalTerm
					{
						ExternalProviderId = externalProvider.Id,
						ExternalId = "abc",
						Name = "Existing Term",
						StartDate = DateTime.UtcNow.AddDays(-30),
						EndDate = DateTime.UtcNow.AddDays(30),
						DateStored = DateTime.UtcNow,
						DateLastUpdated = DateTime.UtcNow
					},
					new TestExternalTerm
					{
						ExternalProviderId = externalProvider.Id,
						ExternalId = "efg",
						Name = "New Term 1",
						StartDate = DateTime.UtcNow.AddDays(30),
						EndDate = DateTime.UtcNow.AddDays(60),
						DateStored = DateTime.UtcNow,
						DateLastUpdated = DateTime.UtcNow
					},
					new TestExternalTerm
					{
						ExternalProviderId = externalProvider.Id,
						ExternalId = "xyz",
						Name = "New Term 2",
						StartDate = DateTime.UtcNow.AddDays(60),
						EndDate = DateTime.UtcNow.AddDays(90),
						DateStored = DateTime.UtcNow,
						DateLastUpdated = DateTime.UtcNow
					}
				};

				var termProviderServiceMapping =
					new Dictionary<Type, Func<ITermProviderService<TestExternalTerm>>>
					{
						{typeof(TestExternalProvider), () =>
						{
							var mockTermProviderService = new Mock<ITermProviderService<TestExternalTerm>>();
							mockTermProviderService
								.Setup(x => x.GetTermsAsync(It.IsAny<Models.ExternalProvider>(), It.IsAny<CancellationToken>()))
								.ReturnsAsync(providedExternalTerms);
							return mockTermProviderService.Object;
						}}
					};

				var service = GetService(dbContext, termProviderServiceMapping);
				await service.SyncTermsAsync();

				var externalTerms = await dbContext.ExternalTerms.ToListAsync();

				Assert.AreEqual(3, externalTerms.Count);
			}
		}

		#endregion SyncTermsAsync

		#region Helpers

		private ITermSyncService GetService(
			TestTermSyncDbContext dbContext,
			IDictionary<Type, Func<ITermProviderService<TestExternalTerm>>> termProviderServiceMapping = null)
		{
			return new TermSyncService<TestExternalTerm>(_logger, dbContext, termProviderServiceMapping ?? _termProviderServiceMapping);
		}

		#endregion Helpers
	}
}