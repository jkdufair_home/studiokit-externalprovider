﻿using StudioKit.Data.Entity.Identity.Models;
using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.DataAccess.Interfaces
{
	public interface IRosterSyncDbContext<TUser, TGroup, TGroupUserRole, TGroupUserRoleLog, TExternalGroup, TExternalGroupUser>
		where TGroup : class, IGroup<TGroupUserRole, TExternalGroup>, new()
		where TGroupUserRole : class, IGroupUserRole, new()
		where TGroupUserRoleLog : class, IGroupUserRoleLog, new()
		where TExternalGroup : class, IExternalGroup, new()
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>, new()
	{
		Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

		Task<IEnumerable<TUser>> GetOrCreateUsersAsync(IEnumerable<TUser> users, CancellationToken cancellationToken = default(CancellationToken));

		#region DbSets

		IDbSet<Role> Roles { get; set; }

		DbSet<UserRole> IdentityUserRoles { get; set; }

		DbSet<TGroup> Groups { get; set; }

		DbSet<TGroupUserRole> GroupUserRoles { get; set; }

		DbSet<TGroupUserRoleLog> GroupUserRoleLogs { get; set; }

		DbSet<Models.ExternalProvider> ExternalProviders { get; set; }

		DbSet<TExternalGroup> ExternalGroups { get; set; }

		DbSet<TExternalGroupUser> ExternalGroupUsers { get; set; }

		#endregion DbSets
	}
}