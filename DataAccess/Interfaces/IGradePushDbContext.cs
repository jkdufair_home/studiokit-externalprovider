﻿using StudioKit.ExternalProvider.Models.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.ExternalProvider.DataAccess.Interfaces
{
	public interface IGradePushDbContext<TGroup, TGroupUserRole, TExternalGroup, TExternalGroupUser, TExternalGroupAssignment, TGroupAssignment, TAssignment, TGroupAssignmentScore, TGroupAssignmentJobLog>
		where TGroup : class, IGroup<TGroupUserRole, TExternalGroup>
		where TGroupUserRole : class, IGroupUserRole
		where TExternalGroup : class, IExternalGroup
		where TExternalGroupUser : class, IExternalGroupUser<TExternalGroup>
		where TExternalGroupAssignment : class, IExternalGroupAssignment, new()
		where TGroupAssignment : class, IGroupAssignment
		where TAssignment : class, IAssignment
		where TGroupAssignmentScore : class, IGroupAssignmentScore
		where TGroupAssignmentJobLog : class, IGroupAssignmentJobLog, new()
	{
		Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));

		#region DbSets

		DbSet<Models.ExternalProvider> ExternalProviders { get; set; }

		DbSet<TGroup> Groups { get; set; }

		DbSet<TExternalGroup> ExternalGroups { get; set; }

		DbSet<TExternalGroupUser> ExternalGroupUsers { get; set; }

		DbSet<TExternalGroupAssignment> ExternalGroupAssignments { get; set; }

		DbSet<TGroupAssignment> GroupAssignments { get; set; }

		DbSet<TAssignment> Assignments { get; set; }

		DbSet<TGroupAssignmentScore> GroupAssignmentScores { get; set; }

		DbSet<TGroupAssignmentJobLog> GroupAssignmentJobLogs { get; set; }

		#endregion DbSets

		#region Methods

		Task<List<TGroupAssignmentScore>> GetScoresAsync(int groupAssignmentId, CancellationToken cancellationToken = default(CancellationToken));

		#endregion Methods
	}
}